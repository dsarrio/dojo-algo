const url_params = new Proxy(new URLSearchParams(window.location.search), {
    get: (searchParams, prop) => searchParams.get(prop),
    set: (searchParams, prop, value) => {
        searchParams.set(prop, value);
        window.history.pushState({}, '', "/?" + searchParams.toString());
        return true;
    },
});

/**
 * Create a new 2D array
 * @param {*} size number of cells
 * @param {*} fn_init function to initialize each cell by converting x,y parameters to cell value;
 * @returns void
 */
function create_grid(size, fn_init) {
    var grid = new Array(size);
    for (var x = 0; x < size; ++x) {
        grid[x] = new Array(size);
        for (var y = 0; y < size; ++y) {
            grid[x][y] = fn_init(x, y);
        }
    }
    return grid;
}

/**
 * Iterate on every cells of the given grid
 * @param {*} grid 
 * @param {*} fn callback for each cell called with (cell, x, y) as parameters
 */
function for_each_cell(grid, fn) {
    for (var x = 0; x < grid.length; ++x) {
        for (var y = 0; y < grid[x].length; ++y) {
            fn({cell:grid[x][y], x, y});
        }
    }
}

/**
 * Shuffle an array in place with provided random generator function
 * @param {*} array the array to shuffle
 * @param {*} fn_rnd function returning random numbers in [0,1] range
 */
function shuffle_array(array, fn_rnd) {
    var count = array.length, randomnumber, temp;
    while (count) {
        randomnumber = fn_rnd() * count-- | 0;
        temp = array[count];
        array[count] = array[randomnumber];
        array[randomnumber] = temp
    }
}

const EMPTY = 0;
const WALL = 1;

class Controller {

    constructor() {
        this.mouseInfosDiv = document.getElementById('mouse_infos');
        this.inputGridSize = document.getElementById('grid_size');
        this.inputRandomSeed = document.getElementById('random_seed');
        this.inputGradient = document.getElementById('gradient');
        this.canvas = document.getElementById("canvas");
        this.canvas_rect = this.canvas.getBoundingClientRect();
        this.canvas.addEventListener('mousemove', (e) => this.onMouseMoved(e));
        this.canvas.addEventListener('click', (e) => this.onMouseClick(e));
        
        this.inputGradient.checked = (url_params.gradient || 0) == 1;
        this.inputGradient.addEventListener('change', (e) => this.onGradientChanged(e.target.checked));

        this.inputRandomSeed.value = url_params.seed || 666;
        this.inputRandomSeed.addEventListener('change', (e) => this.onSeedChanged(e.target.value));

        this.inputGridSize.value = url_params.size || 21;
        this.inputGridSize.addEventListener('change', (e) => this.onGridSizeChanged(e.target.value));

        this.rebuild_maze();
    }

    onSeedChanged(value) {
        url_params.seed = value;
        this.rebuild_maze();
    }

    onGridSizeChanged(value) {
        url_params.size = value;
        this.rebuild_maze();
    }

    onGradientChanged(value) {
        url_params.gradient = value ? 1 : 0;
        this.render();
    }

    onMouseMoved({clientX, clientY}) {
        const grid_step = this.canvas.width / this.inputGridSize.value;
        const cellX = Math.floor((clientX - this.canvas_rect.left) / grid_step);
        const cellY = Math.floor((clientY - this.canvas_rect.top) / grid_step);
        if (cellX >= 0 && cellY >= 0) {
            document.getElementById('cell_coords').innerText = `x: ${cellX}  y: ${cellY}`;
            this.mouseInfosDiv.style.top = (clientY+10) + "px";
            this.mouseInfosDiv.style.left = (clientX+10) + "px";
        }
    }

    onMouseClick({clientX, clientY, altKey}) {
        const grid_step = this.canvas.width / this.inputGridSize.value;
        const cellX = Math.floor((clientX - this.canvas_rect.left) / grid_step);
        const cellY = Math.floor((clientY - this.canvas_rect.top) / grid_step);
        if (altKey) {
            this.grid[cellX][cellY].type = (this.grid[cellX][cellY].type === EMPTY ? WALL : EMPTY);
        } else if (this.grid[cellX][cellY].type === EMPTY) {
            this.targetCell = this.grid[cellX][cellY];
        } else {
            this.targetCell = undefined;
        }
        this.computePath();
    }

    rebuild_maze() {
        this.targetCell = undefined;

        var seed = parseInt(this.inputRandomSeed.value);
        this._rand = () => {
            var t = seed += 0x6D2B79F5;
            t = Math.imul(t ^ t >>> 15, t | 1);
            t ^= t + Math.imul(t ^ t >>> 7, t | 61);
            return ((t ^ t >>> 14) >>> 0) / 4294967296;
        }

        const grid_size = this.inputGridSize.value;

        if (seed === 42) {
            this.grid = create_grid(grid_size, (x,y) => { return {type: EMPTY, x, y}; });
        } else {
            this.grid = create_grid(grid_size, (x,y) => { return {type: WALL, x, y}; });
            const cells = create_grid(grid_size, (x,y) => { return {x, y, neighbors:[]}; });
            for_each_cell(cells, ({cell,x,y}) => {
                if (x % 2 == 0 && y % 2 == 0) {
                    if (x >= 2) cell.neighbors.push(cells[x-2][y]);
                    if (y >= 2) cell.neighbors.push(cells[x][y-2]);
                    if (x < grid_size - 2) cell.neighbors.push(cells[x+2][y]);
                    if (y < grid_size - 2) cell.neighbors.push(cells[x][y+2]);
                    shuffle_array(cell.neighbors, this._rand);
                    this.grid[x][y].type = EMPTY;
                }
            });

            const visited = create_grid(grid_size, () => false);
            var cell = cells[0][0];
            const stack = [];
            stack.push(cell);
            visited[cell.x][cell.y] = true;
            while (stack.length > 0) {
                cell = stack.pop();
                var unvisited_neighbor = undefined;
                for (const n of cell.neighbors) {
                    if (!visited[n.x][n.y]) {
                        unvisited_neighbor = n;
                        break;
                    }
                }
                if (unvisited_neighbor) {
                    stack.push(cell);
                    this.grid[(cell.x + unvisited_neighbor.x) / 2][(cell.y + unvisited_neighbor.y) / 2].type = EMPTY;
                    visited[unvisited_neighbor.x][unvisited_neighbor.y] = true;
                    stack.push(unvisited_neighbor);
                }
            }
        }

        this.computePath();
    }

    computePath() {
        const grid_size = this.inputGridSize.value;

        const nodes = create_grid(grid_size, (x,y) => { return {x, y, neighbors:[]}; });
        for_each_cell(nodes, ({cell,x,y}) => {
            if (x > 0 && this.grid[x-1][y].type === EMPTY) cell.neighbors.push(nodes[x-1][y]);
            if (y > 0 && this.grid[x][y-1].type === EMPTY) cell.neighbors.push(nodes[x][y-1]);
            if (x < grid_size - 1 && this.grid[x+1][y].type === EMPTY) cell.neighbors.push(nodes[x+1][y]);
            if (y < grid_size - 1 && this.grid[x][y+1].type === EMPTY) cell.neighbors.push(nodes[x][y+1]);
        });

        this.path = main(grid_size, nodes[0][0], this.targetCell ? nodes[this.targetCell.x][this.targetCell.y] : undefined);
        this.render();
    }

    render() {
        const ctx = canvas.getContext("2d");
        const grid_step = canvas.width / this.inputGridSize.value;
    
        ctx.fillStyle = "#FFF";
        ctx.fillRect(0, 0, canvas.width, canvas.height);
    
        for_each_cell(this.grid, ({cell, x, y}) => {
            if (cell.type !== EMPTY) {
                ctx.fillStyle = cell.type === WALL ? "#000" : cell.type === HIDDEN ? "#AAA" : cell.color;
                const px = Math.floor(x * grid_step);
                const py = Math.floor(y * grid_step);
                const npx = Math.floor((x + 1) * grid_step);
                const npy = Math.floor((y + 1) * grid_step);
                ctx.fillRect(px, py, npx-px, npy-py);
            }
        });

        for (var i = 0; i < this.path.length; i++) {
            const n = this.path[i];
            if (this.grid[n.x][n.y].type != EMPTY) {
                ctx.fillStyle = "#FF0000";
            } else {
                if (this.inputGradient.checked) {
                    const clamp = (v, min, max) => v < min ? min : v < max ? v : max;
                    const f = (i+1) / this.path.length + 0.25;
                    const green = Math.floor(clamp(f, 0, 1) * 255);
                    const color = "#11" + green.toString(16).padStart(2,'0').toUpperCase() + green.toString(16).padStart(2,'0').toUpperCase();
                    ctx.fillStyle = color;
                } else {
                    ctx.fillStyle = "#2222FF";
                }
            }
            const px = Math.floor(n.x * grid_step);
            const py = Math.floor(n.y * grid_step);
            const npx = Math.floor((n.x + 1) * grid_step);
            const npy = Math.floor((n.y + 1) * grid_step);
            ctx.fillRect(px, py, npx-px, npy-py);
        }

        if (this.targetCell) {
            ctx.fillStyle = "#1111FF";
            const px = Math.floor(this.targetCell.x * grid_step);
            const py = Math.floor(this.targetCell.y * grid_step);
            const npx = Math.floor((this.targetCell.x + 1) * grid_step);
            const npy = Math.floor((this.targetCell.y + 1) * grid_step);
            ctx.fillRect(px, py, npx-px, npy-py);
        }
    }
}
