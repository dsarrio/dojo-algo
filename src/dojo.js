/**
 * @param {*} grid_size la taille de la grille (toujours carrée)
 * @param {*} root_node le premier noeud duquel démarrer l'exploration {x: Int, y: Int, neighbors: Node}
 * @param {*} end_node (optional) le noeud cible placé avec la souris {x: Int, y: Int, neighbors: Node}
 * @returns un tableau de noeuds
 */
 function main(grid_size, root_node, end_node) {    
    const path_nodes = [root_node];

    // explorer à partir de root_node le labyrinthe et remplir path_node avec la liste des noeuds rencontrés.

    return path_nodes;
}
